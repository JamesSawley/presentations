import React from 'react';

const notes = `
 * Declarative = JSX
`;

export default class Slide extends React.Component {
  static notes = notes;

  render() {
    return (
      <div>
        <h1>Why use it?</h1>
        <ul>
          <li>Declarative</li>
          <li>The Virtual DOM</li>
          <li>Component based</li>
          <li>One-way data flow</li>
        </ul>
      </div>
    );
  }
}
