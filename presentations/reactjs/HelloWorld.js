import React from 'react';
import Codepen from 'react-codepen-embed';

const code = `
`

const notes = `
`;

export default class Slide extends React.Component {
  static notes = notes
  render() {
    return (
      <div>
        <h1>Hello React!</h1>
        <p style={{width: '900px'}}></p>
        <Codepen user="sawdust1993" hash="vWNXQP" height={500} preview={false} defaultTab='html,result'></Codepen>
      </div>
    );
  }
}
