import React from 'react';

const notes = `
 * Declarative = JSX
`;

export default class Slide extends React.Component {
  static notes = notes;

  render() {
    return (
      <div>
        <h1>You understand the basics of:</h1>
        <ul>
          <li>HTML / Javascript syntax</li>
          <li>Flux Architecture</li>
        </ul>
      </div>
    );
  }
}
