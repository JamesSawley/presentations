import React from 'react';

const notes = `
`;

export default class Slide extends React.Component {
  static notes = notes;

  render() {
    return (
      <div>
        <h1>Reasons not to use React</h1>
        <ul>
          <li>Not supported by browsers below IE8</li>
          <li>You need other tooling</li>
          <li>React is optimised for frequent rendering</li>
          <li><del>Legal - licensing</del></li>
        </ul>
      </div>
    );
  }
}
