import React from 'react';
import CodeBlock from '../../lib/components/CodeBlock';

const code = `
tick() {
  this.setState({
    date: new Date()
  });
}

this.timerID = setInterval(
  () => this.tick(),
  1000
);
`

const notes = `
 * remember to make fun of angular
 * more notes
`;

class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {date: new Date()};
  }
  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }
  componentWillUnmount() {
    clearInterval(this.timerID);
  }
  tick() {
    this.setState({
      date: new Date()
    });
  }
  render() {
    return (
      <div>
        <h2>{this.state.date.toLocaleTimeString()}</h2>
      </div>
    );
  }
}

export default class Slide extends React.Component {
  static notes = notes

  render() {

    return (
      <div>
        <Clock />
        <CodeBlock>
          { code }
        </CodeBlock>
      </div>
    );
  }
}
