import React from 'react';
import CodeBlock from '../../lib/components/CodeBlock';

const code = `
this.timerID = setInterval(
  () => this.tick(),
  1000
);
`

const notes = `
 * remember to make fun of angular
 * more notes
`;

export default class Slide extends React.Component {
  static notes = notes

  render() {

    return (
      <div>
        <h1>What Next?</h1>
        <ul>
          <li>React Native</li>
          <li>React XP</li>
          <li>Preact</li>
        </ul>
      </div>
    );
  }
}
