import React from 'react';
import CodeBlock from '../../lib/components/CodeBlock';

const code = `
class Hello extends React.Component {
  render() {
    return (
      <div>
        <h1>Hello world</h1>
        <p>Using JSX is easy!</p>
      </div>
    );
  }
}
`

const notes = `
 * remember to make fun of angular
 * more notes
`;

export default class Slide extends React.Component {
  static notes = notes
  render() {
    return (
      <div>
        <h1>Declarative</h1>
        <p>JSX makes your code more predictable and easier to debug.</p>
        <CodeBlock>
          { code }
        </CodeBlock>
        <p>
          <a href="http://babeljs.io/repl#?babili=false&browsers=&build=&builtIns=false&code_lz=MYGwhgzhAEASCmIQHsCy8pgOb2vAHgC7wB2AJjAErxjCEB0AwsgLYAOyJph0A3gFABIAE6ky8YQAoAlHyEj4hAK7CS0ADxkAlgDcAfAiTI-hABZaI9NsORtLJMC3gBfdQHpt-gNxDn_P_zUtIQAIgDyqPSi5BKS6oYo6Jg40A5OALwARCHwOlokmdBuegA00CzISiSEAHLI4tJeQA&debug=false&circleciRepo=&evaluate=false&lineWrap=false&presets=react&prettier=true&targets=&version=6.26.0">
            See how JSX compiles
          </a>
        </p>
      </div>
    );
  }
}
