import React from 'react';
import Codepen from 'react-codepen-embed';
import CodeBlock from '../../lib/components/CodeBlock';

const code = `
class Component extends React.Component {
  constructor(){
    super()
    this.state= { name: '' }
    this.change = this.change.bind(this);
  }

  change(e) {
    this.setState({ name: e.target.value });
  }

  render() {
    return (
      <div>
        <input type="text" onChange={this.change} />
        <h1>Hello {this.state.name}</h1>
      </div>
    );
  }
}
`

const notes = `
`;

export default class Slide extends React.Component {
  static notes = notes
  render() {
    return (
      <div>
        <h1>Data Flow</h1>
        <p>React implements a strict one-way data flow.</p>
        <p style={{width: '900px'}}></p>
        <Codepen user="sawdust1993" hash="GOpZxq" height={500} preview={false} defaultTab='js,result'></Codepen>
      </div>
    );
  }
}
