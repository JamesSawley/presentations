import React from 'react';
import CodeBlock from '../../lib/components/CodeBlock';
import imgSrc from './images/virtual-dom.png';

const notes = `
HTML is super slow, JS is lightning quick.

Here's what happens when you try to update the DOM in React:
* The entire virtual DOM gets updated.
* The virtual DOM gets compared to what it looked like before you updated it. React figures out which objects have changed.
* The changed objects, and the changed objects only, get updated on the real DOM.
* Changes on the real DOM cause the screen to change.
`;

export default class Slide extends React.Component {
  static notes = notes

  render() {

    return (
      <div>
        <h1>Virtual DOM</h1>
        <p>A lightweight javascript representation of the DOM.</p>
        <img src={ imgSrc } style={{height:'300px'}}/>
      </div>
    );
  }
}
