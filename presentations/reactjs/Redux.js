import React from 'react';
import imgSrc from './images/redux-architecture.gif';

const notes = `
`;

export default class Slide extends React.Component {
  static notes = notes

  render() {
    return (
      <div>
        <h1>Redux</h1>
        <h3><a href="http://redux.js.org/">redux.js.org</a></h3>
        <p>Redux is a predictable state container for JavaScript apps.</p>
        <img src={ imgSrc } style={{height:'400px'}}/>
      </div>
    );
  }
}
