import React from 'react';
import Codepen from 'react-codepen-embed';
import CodeBlock from '../../lib/components/CodeBlock';

const code = `
class Hello extends React.Component {
  render() {
    return (
      <div>
        <h1>Hello world</h1>
        <p>Using JSX is easy!</p>
      </div>
    );
  }
}

class App extends React.Component {
  render() {
    return (
      <div>
        <Hello />
      </div>
    );
  }
}
`

const notes = `
`;

export default class Slide extends React.Component {
  static notes = notes
  render() {
    return (
      <div>
        <h1>Component Based</h1>
        <p style={{width: '900px'}}></p>
        <Codepen user="sawdust1993" hash="dZYPqQ" height={500} preview={false} defaultTab='js,result'></Codepen>
      </div>
    );
  }
}
