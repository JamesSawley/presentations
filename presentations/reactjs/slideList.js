import React from 'react';

import Intro from './Intro';
import Assumptions from './Assumptions';
import What from './What';
import HelloWorld from './HelloWorld';
import Bad from './Bad';
import Why from './Why';
import Declarative from './Declarative';
import VirtualDom from './VirtualDom';
import VirtualDomExample from './VirtualDomExample';
import Component from './Component';
import DataFlow from './DataFlow';
import Redux from './Redux';
import Redux2 from './Redux2';
import WhatNext from './WhatNext';
import Questions from './Questions';

const slideComponents = [
  Intro,
  What,
  HelloWorld,
  Bad,
  Why,
  Declarative,
  VirtualDom,
  VirtualDomExample,
  Component,
  DataFlow,
  Redux,
  WhatNext,
  Questions
];

export default slideComponents.map((SlideComponent, idx) => {
  return <SlideComponent key={ idx } />;
});
