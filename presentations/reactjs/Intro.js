import React from 'react';
import logo from './images/logo.png';

const notes = `
 * remember to make fun of angular
 * more notes
`;

export default class Slide extends React.Component {
  static notes = notes

  componentDidMount() {
    let styleSheet = document.styleSheets[0];
    let keyframes =
    `@-webkit-keyframes App-logo-spin {
      from { -webkit-transform: rotate(0deg) }
      to { -webkit-transform: rotate(360deg) }
    }`;
    styleSheet.insertRule(keyframes, styleSheet.cssRules.length);
  }

  render() {
    let style = {
      height: '80px',
      animation: 'App-logo-spin infinite 20s linear'
    };

    return (
      <div>
        <h1>React</h1>
        <img src={ logo } style={ style }/>
        <h2><a href='https://reactjs.org/'>reactjs.org</a></h2>
        <h3>James Sawley</h3>
      </div>
    );
  }
}
