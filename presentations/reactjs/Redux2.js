import React from 'react';
import imgSrc from './images/redux-architecture-2.gif';

const notes = `

`;

export default class Slide extends React.Component {
  static notes = notes

  render() {
    return (
      <div>
        <h1>Redux, with middleware</h1>
        <img src={ imgSrc } style={{height:'400px'}}/>
      </div>
    );
  }
}
